function countLetter(letter, sentence) {
    let result = 0;

    // Check first whether the letter is a single character.
    // If letter is a single character, count how many times a letter has occurred in a given sentence then return count.

    if (letter.length === 1){
   
        for (let i = 0; i < sentence.length; i++) {
            if (sentence[i].toLowerCase() === letter.toLowerCase()) {
                result++;
            }
        }
        return result;
    } else {
    // If letter is invalid, return undefined.
        return undefined;
    }
}


function isIsogram(text) {
    // An isogram is a word where there are no repeating letters.

    // The function should disregard text casing before doing anything else.
    const lowercaseText = text.toLowerCase();
    const isogramCheck = {};

    // If the function finds a repeating letter, return false. Otherwise, return true.
    for (let i = 0; i < lowercaseText.length; i++) {
        const chars = lowercaseText[i];

        if (isogramCheck[chars]) {
            return false;
        }
        isogramCheck[chars] = true;
    }
    return true;
}

function purchase(age, price) {
  
    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;
    }
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
     else if (age >= 13 && age <= 21 || age >= 65) {
        const discountedPrice = price * 0.8;
        const roundedPrice = discountedPrice.toFixed(2);
        return roundedPrice;
    }
    // Return the rounded off price for people aged 22 to 64.
    else {
        const roundedPrice = price.toFixed(2);
    // The returned value should be a string.
        return roundedPrice;
    }

    // toFixed(2) used, Math.round does not work to roundoff the prices
    //OR
    // switch (true) {
    //     // Return undefined for people aged below 13.
    //     case age < 13:
    //         return undefined;
    //     // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    //     case age >= 13 && age <= 21 || age >= 65:
    //         const discountedPrice = (price * 0.8).toFixed(2);
    //         return discountedPrice;
    //     // Return the rounded off price for people aged 22 to 64.
    //     default:
    //     // The returned value should be a string.
    //         const roundedPrice = price.toFixed(2);
    //         return roundedPrice;
    // }

}

function findHotCategories(items) {
    // Find categories that has no more stocks.
    // const categories = [...new Set(items.map(item => item.category))];

    // The hot categories must be unique; no repeating categories.
    // const hotCategories = categories.filter(category => 
    //     items.some(item => item.category === category && item.stocks === 0)
    // );
    
    //OR
    const hotCategories = items.reduce((categories, item) => {
        if (item.stocks === 0 && !categories.includes(item.category)) {
            categories.push(item.category);
        }
        return categories;
    }, []);

    // The passed items array from the test are the following:
    // { id: 'tltry001', name: 'soap', stocks: 14, category: 'toiletries' }
    // { id: 'tltry002', name: 'shampoo', stocks: 8, category: 'toiletries' }
    // { id: 'tltry003', name: 'tissues', stocks: 0, category: 'toiletries' }
    // { id: 'gdgt001', name: 'phone', stocks: 0, category: 'gadgets' }
    // { id: 'gdgt002', name: 'monitor', stocks: 0, category: 'gadgets' }

    // The expected output after processing the items array is ['toiletries', 'gadgets'].
    // Only putting return ['toiletries', 'gadgets'] will not be counted as a passing test during manual checking of codes.
    
    return hotCategories;
}

function findFlyingVoters(candidateA, candidateB) {

    const commonVoters = [];

    for (let i = 0; i < candidateA.length; i++) {
        for (let j = 0; j < candidateB.length; j++) {
            if (candidateA[i] === candidateB[j]) {
                commonVoters.push(candidateA[i]);
                break; // Once a match is found, break out of the inner loop
            }
        }
    }

    // //  const set1 = new Set([1, 2, 3, 4, 5]);
    // const setA = new Set(candidateA);
    // const setB = new Set(candidateB);

    // // Find voters who voted for both candidate A and candidate B.
    // const commonVoters = [...setA].filter(voter => setB.has(voter));

    // The passed values from the test are the following:
    // candidateA: ['LIWf1l', 'V2hjZH', 'rDmZns', 'PvaRBI', 'i7Xw6C', 'NPhm2m']
    // candidateB: ['kcUtuu', 'LLeUTl', 'r04Zsl', '84EqYo', 'V2hjZH', 'LIWf1l']

    // The expected output after processing the candidates array is ['LIWf1l', 'V2hjZH'].
    // Only putting return ['LIWf1l', 'V2hjZH'] will not be counted as a passing test during manual checking of codes.
    
    return commonVoters;
}

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};