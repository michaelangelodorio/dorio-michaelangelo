
import { useState } from 'react';
import { Button, Modal, Form } from 'react-bootstrap';
import Swal from 'sweetalert2';



export default function ArchiveCourse({course, fetchData, isActive}){

	const archiveToggle = (e, course)=>{

		e.preventDefault();

		fetch(`http://localhost:4000/courses/${course}/archive`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if(data === true){
				Swal.fire({
					title:'Success!',
					icon:'success',
					text:'Course Successfully Archived'
				})
				
                fetchData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchData();
			}
		})

	}

	const activateToggle = (e, course)=>{

		e.preventDefault();

		fetch(`http://localhost:4000/courses/${course}/activate`,{

			method:'PUT',
			headers:{
				'Content-Type':'application/json',
				'Authorization':`Bearer ${localStorage.getItem('token')}`
			}
			
		})
		.then(res=>res.json())
		.then(data=>{
			console.log(data)

			if(data === true){
				Swal.fire({
					title:'Success!',
					icon:'success',
					text:'Course Successfully Activated'
				})
				
                fetchData();
			}else{
				Swal.fire({
					title:'Error!',
					icon:'error',
					text:'Please try again'
				})
				
                fetchData();
			}
		})

	}

	return (
		<>

		{isActive ? 
			<Button variant="danger" size="sm" onClick={e=>archiveToggle(e,course)}>Archive</Button>
			:
			<Button variant="success" size="sm" onClick={e=>activateToggle(e,course)}>Activate</Button>	            
		}

		</>


	)
}