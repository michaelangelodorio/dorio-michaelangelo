import React from 'react';

// Creates a Context object

const UserContext = React.createContext();

// Provider component
export const UserProvider = UserContext.Provider;

export default UserContext;