// Use the "require" directive to load Node.js modules
const http = require('http');
// Creates a variable "port" to store the port number
const port = 3000;

// Creates a variable "app" that stores the output of the "createServer" method
const app = http.createServer((request, response) => {

    // Accessing the "login" route returns a message of "Welcome to the login page"
    if (request.url == '/login') {

        response.writeHead(200, {'Content-Type': 'text/plain'})
        response.end('Welcome to the login page.')

    // All other routes will return a message of "I'm sorry the page you are looking for cannot be found"
    } else {

        // Set a status code for the response - a 404 means Not Found
        response.writeHead(404, {'Content-Type': 'text/plain'})
        response.end(`I'm sorry the page you are looking for cannot be found.`)

    }

})

// Uses the "server" and "port" variables created above.
app.listen(port);

// When server is running, console will print the message:
console.log(`Server now accessible at localhost:${port}.`);

module.exports =  {app}